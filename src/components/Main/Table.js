import React from 'react';
import {getRequest} from './requests';
import axios from "axios"
const link = 'http://178.128.196.163:3000'

const Table = ({data, setMyData}) => {
    const deleteRequest = (id) => {
        console.log(id.target.value, 'hi')
        const withoutThisId = (id) => {
            return data.filter((unit) => unit._id !== id)
        }
        async function f() {
            await axios.delete(`${link}/api/records/${id.target.value}`)
                .then((responce) => console.log(responce))
                .then(setMyData(withoutThisId(id.target.value)))
                .catch((e) => console.log(e))
            await getRequest().then(result => setMyData(result))
                .catch((e) => console.log(e))
        } 
        f()
    }
    
    const tableBody = data.map((unit) => {
        if((unit.name && unit.age) !== undefined) {
            return (
                <tr>
                    <td>{unit.name}</td>
                    <td>{unit.age}</td>
                    <button>delete</button>
                </tr>
            )
        }
        if((unit.data !== undefined)) {
            const reductableUnitRender = () => {
                const newUnit = {name: unit.data.name,
                age: unit.data.age}
                const onNameChangeHandler = (e) => {
                    newUnit.name = e.currentTarget.value
                }
                const onAgeChangeHandler = (e) => {
                    newUnit.age = e.currentTarget.value
                }
                const saveUnit = (id) => {
                    const ID = id.target.value
                    const newData = data.map((unit) => {
                        if (unit._id === ID) {
                            unit.reduct = !unit.reduct
                        }
                        return unit
                    })
                    async function f() {
                        const data = newUnit
                            return await axios.post(`${link}/api/records/${ID}`,{data})
                                .then(getRequest().then(result => setMyData(result.data)))
                                .catch((e) => console.log(e))
                    }
                    f()
                    setMyData(newData)
                }
                return (
                    <tr>
                        <td><input onChange={onNameChangeHandler} placeholder={`${unit.data.name}`}></input></td>
                        <td><input onChange={onAgeChangeHandler} placeholder={`${unit.data.age}`}></input></td>
                        <button class='hi' onClick={(e) => deleteRequest(e)} value={unit._id}>Delete</button>
                        <button  class='hi' onClick={saveUnit} value={unit._id}>Save</button>
                    </tr>
                        
                )
            }
            const unreductableUnitRender = () => {
                return (
                    <tr>
                        <td>{unit.data.name}</td>
                        <td>{unit.data.age}</td>
                        <button class='hi' onClick={(e) => deleteRequest(e)} value={unit._id}>Delete</button>
                        <button class='hi' onClick={reductable} value={unit._id}>Redact</button>
                    </tr>    
                )
            }
            const reductable = (id) => {
                const ID = id.target.value
                const newData = data.map((unit) => {
                    if (unit._id === ID) {
                        unit.reduct = !unit.reduct
                    }
                    return unit
                })
                setMyData(newData)
            }
            
            return (unit.reduct ? reductableUnitRender() : unreductableUnitRender())
        }
        return (
            <tr>
                <td>'uncorrect data'</td>
                <td>id:${unit._id}</td>
                <button>delete</button>
            </tr>
        )

        
    })
    return tableBody
}

export default Table