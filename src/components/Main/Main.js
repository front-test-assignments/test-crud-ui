import React, { useState } from 'react';
import Table from './Table';
import {getRequest} from './requests';
import axios from "axios"
const link = 'http://178.128.196.163:3000'

const deleteRequest = (id) => {
    
    async function f() {
        await axios.delete(`${link}/api/records/:${id.target.value}`)
            .catch((e) => console.log(e))
    }
    f()
}

const Main = () => {
    const [myData, setMyData] = useState()
    const normalizeData = (data) => {
        const newData = [...data]
        newData.map((unit) => unit['reduct'] = false)
        setMyData(newData)
    }
    if (!myData) {
        Promise.resolve(getRequest().then(result => normalizeData(result))
            .catch((e) => console.log(e)))
    }

    const addData = () => {
        const newData = {
                name: 'simple',
                age: 113,
        }
        addRequest(newData).then((e) => setMyData([...myData, e]))
    }

    const addRequest = (data) => {
        async function f() {
            return await axios.put(`${link}/api/records`, {data})
                .then((responce) => (responce.data))
                .catch((e) => console.log(e))
        }
        return  f()
    }
    
    const addDataButton = () => {
        return (
            <button onClick={addData}>Add New</button>
        )
    }

    const dataRender = () => {
        return (
            <>
                <table>
                    <tr>
                        <th>Name</th>
                        <th>Age</th>
                    </tr>
                    <Table data={myData} setMyData={setMyData}/>
                </table>
                {addDataButton()}
            </>
        )
    }

    const noDataRender = () => {
        return (
            <>
                <table>
                <tr>
                    <th>Name</th>
                    <th>Age</th>
                </tr>
                </table>
                {addDataButton()}
            </> 
        )
    }

    return myData ? dataRender() : noDataRender()
}

export { Main, deleteRequest}