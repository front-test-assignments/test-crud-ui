import axios from "axios"
const link = 'http://178.128.196.163:3000'

const getRequest = () => {
    async function f() {
        return await axios.get(`${link}/api/records`).then((responce) => responce.data)
    }
    return f()
}

export {getRequest}